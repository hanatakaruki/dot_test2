describe('Checkout test', function(){
    it ('Checkout',function(){
        cy.visit('http://automationpractice.com/')

        cy.get('#block_top_menu > ul > li:nth-child(3) > a').click()
        cy.get('#center_column > ul > li > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default').click()
        cy.wait(1000)
        cy.get('.continue > span').click()

        cy.get('.sf-menu > :nth-child(2) > .sf-with-ul').click()
        cy.get('.first-in-line.first-item-of-tablet-line > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click()
        cy.wait(1000)
        cy.get('.continue > span').click()
        cy.get(':nth-child(2) > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click()
        cy.wait(1000)
        cy.get('.continue > span').click()

        cy.get('.sf-menu > :nth-child(1) > [href="http://automationpractice.com/index.php?id_category=3&controller=category"]').click()
        cy.get(':nth-child(1) > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click()
        cy.wait(1000)
        cy.get('.continue > span').click()
        cy.get('#center_column > ul > li:nth-child(2) > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default').click()
        cy.wait(1000)
        cy.get('.continue > span').click()
        cy.get('#center_column > ul > li.ajax_block_product.col-xs-12.col-sm-6.col-md-4.last-in-line.first-item-of-tablet-line.last-item-of-mobile-line > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default').click()
        cy.wait(1000)
        cy.get('.continue > span').click()

        cy.get('#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a').click()

        cy.get('#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium').click()

        cy.get('#email').type('ahmadubaidillah5@gmail.com')
        cy.get('#passwd').type('nokia2300')
        cy.get('#SubmitLogin').click()

        cy.get('#center_column > form > p > button').click()
        cy.get('#cgv').click()
        cy.get('#form > p > button').click()
    })
})
