describe('Edit Profile test', function(){
    it ('Edit profile',function(){
        cy.visit('http://automationpractice.com/')
    
        cy.get('#header > div.nav > div > div > nav > div.header_user_info > a').click()
    
        cy.get('#email').type('ahmadubaidillah5@gmail.com')
        cy.get('#passwd').type('nokia2300')
        cy.get('#SubmitLogin').click()
    
        cy.get('#center_column > div > div:nth-child(1) > ul > li:nth-child(3) > a').click()
    
        cy.get('#center_column > div.clearfix.main-page-indent > a').click();
        


        cy.get('#address1').type('jalan galunggung')
        cy.get('#address2').type('jalan brawijaya')
        cy.get('#city').type('malang')
        cy.get('#id_state').select('2')
        cy.get('#postcode').type('87182')
        cy.get('#phone').type('0919201')
        cy.get('#phone_mobile').type('9187291')
        cy.get('#other').type('test')
        cy.get('#alias').clear()
        cy.get('#alias').type('Silicon valley')
        cy.get('#submitAddress').click()

        cy.get('#center_column > div.addresses > div > div:nth-child(2) > ul > li.address_update > a:nth-child(1)').click()
    
        cy.get('#address1').clear()
        cy.get('#address1').type('jalan jalan')
        cy.get('#address2').clear()
        cy.get('#address2').type('jalan bareng')
        cy.get('#city').clear()
        cy.get('#city').type('surakarta')
        cy.get('#id_state').select('3')
        cy.get('#postcode').clear()
        cy.get('#postcode').type('19211')
        cy.get('#phone').clear()
        cy.get('#phone').type('1910112')
        cy.get('#phone_mobile').clear()
        cy.get('#phone_mobile').type('9187291')
        cy.get('#other').clear()
        cy.get('#other').type('test lagi')
        cy.get('#alias').clear()
        cy.get('#alias').type('Silicon hp')
        cy.get('#submitAddress').click()

        cy.wait(1000)
        cy.get('#center_column > div.addresses > div > div:nth-child(2) > ul > li.address_update > a:nth-child(2)').click()

    })
})
    